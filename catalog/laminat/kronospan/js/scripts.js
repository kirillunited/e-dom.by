$(function () {
    var noPl = $('.no-pl-head');
    var searchWrap = $('.search-head-3');
    var infoTel = $('.head-call .info_tel');
    var infoTime = $('.head-call .info_time');
    $('body').on('focus', '.search-wrap input[type=search]', function () {
        $(this).parent()
            .toggleClass('control-group_border');
        searchWrap.toggleClass('search-head-3_focus');
    });
    $('body').on('blur', '.search-wrap input[type=search]', function () {
        $(this).parent()
            .toggleClass('control-group_border');
        searchWrap.toggleClass('search-head-3_focus');
    });

    $(document).on('scroll', function (e) {
        $('.info-wrap__dropdown').addClass('hidden');
        if ($('.g-header_i').hasClass('fixed')) {
            noPl.removeClass('no-pl_custom');
            infoTel
                .find('.info__desc')
                .addClass('hidden')
                .parent()
                .find('.sub-phone')
                .addClass('hidden');
            infoTel.find('.info__desc_min')
                .addClass('show_inline');
            infoTime
                .find('.info__desc')
                .addClass('hidden')
                .parent()
                .find('.main-text')
                .addClass('hidden')
                .parent()
                .find('.time_min').addClass('show_inline');
            $('.info-wrap').addClass('info-wrap_min');
            $('.info_mail').appendTo('.info-wrap__dropdown');
        } else {
            noPl.addClass('no-pl_custom');
            infoTel
                .find('.info__desc')
                .removeClass('hidden')
                .parent()
                .find('.sub-phone')
                .removeClass('hidden');
            infoTel.find('.info__desc_min')
                .removeClass('show_inline');
            infoTime
                .find('.info__desc')
                .removeClass('hidden')
                .parent()
                .find('.main-text')
                .removeClass('hidden')
                .parent()
                .find('.time_min').removeClass('show_inline');
            $('.info-wrap').addClass('info-wrap_min');
            $('.info_mail').appendTo('.info-wrap');
        }
    });
    $('body').on('click', '.info_tel .arrow', function () {
        $('.info-wrap__dropdown').toggleClass('hidden')
            .find('.info_active_call .info__desc').show();
    });
    $(window).resize(function () {
        $('.info-wrap__dropdown').addClass('hidden');
        if ($(window).width() < 479) {
            $('.current-price .currency').text('руб.');
        } else {
            $('.current-price .currency').text('');
        }
    });
    if ($(window).width() < 479) {
        $('.current-price .currency').text('руб.');
    } else {
        $('.current-price .currency').text('');
    }
    $('body').on('click', '.add-accessories', function () {
        var scrollTarget = $(this).attr('href');
        var navHeight = $('.head_bot').outerHeight();
        console.log(navHeight);
        $('body, html').animate({
            scrollTop: $(scrollTarget).offset().top - navHeight
        }, 1000);
        return false;
    });
    // close block on click outside
    $(document).click(function (e) {
        if (!$('.info-wrap__dropdown').is(e.target) && $('.info-wrap__dropdown').has(e.target).length === 0 && !$('.arrow').is(e.target)) {
            $('.info-wrap__dropdown').addClass('hidden');
        };
    });
});

//счётчик товара в списке товаров

function countMinus(elem, event) {
    event.stopPropagation();
    var input = elem.parent().find('input');
    var count = parseInt(input.val()) - 1;
    if (count < 1) {
        count = 1;
    }
    input.val(count);
    input.change();
    return false;
}

function countPlus(elem, event) {
    event.stopPropagation();
    var input = elem.parent().find('input');
    input.val(parseInt(input.val()) + 1);
    input.change();
    return false;
}