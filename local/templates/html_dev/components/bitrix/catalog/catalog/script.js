$(function () {
    $(document).delegate('.catalog-sort .item > a', 'click', function (e) {
        e.preventDefault();
        var field = $(this).data('field');
        var order = $(this).data('order');
        if (order == 'reset'){
            $.removeCookie('SORT_CATALOG', { path: '/' });
        } else {
            $.cookie("SORT_CATALOG", field + ',' + order, { path: '/', expires: 365 });
        }
        window.location.reload();
    });

    $(document).delegate('.catalog-sort a.view-item', 'click', function (e) {
        e.preventDefault();
        var template = $(this).data('template');
        $.cookie("TEMPLATE_CATALOG", template, { path: '/', expires: 365 });
        window.location.reload();
    });

    $(document).delegate('form.smartfilter input[type="text"],form.smartfilter input[type="checkbox"],form.smartfilter input[type="radio"],form.smartfilter select',
        'change', function (e) {
        reloadSection();
    });
});

var smartFilterXHR;
function reloadSection() {
    $("body").css("cursor", "wait");
    var $smartfilter = $('form.smartfilter');
    if(smartFilterXHR && smartFilterXHR.readyState != 4){
        smartFilterXHR.abort();
    }
    smartFilterXHR = $.ajax({
        data: $smartfilter.serialize()+'&set_filter=Y',
        success: function (data) {
            if (typeof data.section !== 'undefined'){
                $('.catalog-list-ajax').html(data.section);
            }
            /*if (typeof data.filter_link !== 'undefined' &&
                window.location.href.indexOf('?q=') === -1 &&
                window.location.href.indexOf('&q=') === -1){
                showUrlInBrowser(data.filter_link);
            }*/
            $("body").css("cursor", "");
            $(window).resize();
        }
    });
}